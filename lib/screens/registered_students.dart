import 'package:flutter/material.dart';
import 'package:sql_concept/data_base/database_mangement.dart';
import 'package:sql_concept/widgets/student_list_widget.dart';
class StudentsLists extends StatefulWidget {
  const StudentsLists({Key? key}) : super(key: key);

  @override
  _StudentsListsState createState() => _StudentsListsState();
}

class _StudentsListsState extends State<StudentsLists> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green[200],
        title: const Text('Students List'),
      ),
          body: FutureBuilder(
        future: DataBaseManagement.instance.getAllStudents(),
        builder: (BuildContext context, AsyncSnapshot snapshot){
          if(snapshot.hasData){
            if(snapshot.data.isEmpty){
              return const Center(child: Text('No Student has been added yet...!'),);
            }else{
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index){
                    var std = snapshot.data[index];
                    return StudentListWidget(studentModel: std, update: (){
                      return
                    }, delete: (){});
                  });
            }
          }else{
            return Center(child: Row(
              children: const [
                Text('Loading....  '),
                CircularProgressIndicator(),
              ],
            ));
          }
        }),
    );
  }
}
