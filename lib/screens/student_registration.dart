import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sql_concept/data_base/database_mangement.dart';
import 'package:sql_concept/model/student_model.dart';

class StudentRegistration extends StatefulWidget {
  const StudentRegistration({Key? key}) : super(key: key);

  @override
  State<StudentRegistration> createState() => _StudentRegistrationState();
}

class _StudentRegistrationState extends State<StudentRegistration> {
  var formKey = GlobalKey<FormState>();
  late String name;
  late String fName;
  late int rollNo;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register a Student'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(18),
        child: Form(
            key: formKey,
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Name',
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide:
                          const BorderSide(color: Colors.black, width: 2),
                    ),
                  ),
                  validator: (value) {
                    name = value!;
                    if (value.isNotEmpty) {
                      return null;
                    } else {
                      return 'Please provide name';
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Father Name',
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(color: Colors.black, width: 2),
                    ),
                  ),
                  validator: (value) {
                    fName = value!;
                    if (value.isNotEmpty) {
                      return null;
                    } else {
                      return 'Please provide Father name';
                    }
                  },
                ),
                const SizedBox(
                  height: 10,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Roll No',
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(color: Colors.black, width: 2),
                    ),
                  ),
                  validator: (value) {
                    rollNo = int.parse(value!);
                    if (value.isNotEmpty) {
                      return null;
                    } else {
                      return 'Please provide roll No';
                    }
                  },
                ),
                const SizedBox(
                  height: 17,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        if (formKey.currentState!.validate()) {
                          var std = StudentModel(
                              name: name,
                              fName: fName,
                              rollNo: rollNo.toString());
                          int result = await DataBaseManagement.instance
                              .insertStudent(std);
                          if (result > 0) {
                            formKey.currentState?.reset();
                            Fluttertoast.showToast(
                              msg: 'Saved Successfully',
                              backgroundColor: Colors.green,
                              fontSize: 20,
                            );
                          } else {
                            Fluttertoast.showToast(
                                msg: 'Not Saved',
                                backgroundColor: Colors.red,
                                fontSize: 20);
                          }
                        }
                      },
                      child: const Text('Save Record'),
                    ),
                    ElevatedButton(
                      onPressed: () {},
                      child: const Text('View Record'),
                    ),
                  ],
                )
              ],
            )),
      ),
    );
  }
}
