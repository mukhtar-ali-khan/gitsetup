import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sql_concept/model/student_model.dart';

class DataBaseManagement {
  // we need a constructor to create instance of database
  DataBaseManagement._privateConstructor(); // named constructor
  //now we need to initialize the class DataBaseManagement
  // we initialize this with an object of the class by assigning it the constructor

  static final DataBaseManagement instance =
      DataBaseManagement._privateConstructor();
  // now we need actual Database object so..
  static Database? _database;
  // create a getter for database
  Future<Database> get database async {
    _database ??= await initializeDatabase();
    return _database!;
  }

  Future<Database> initializeDatabase() async {
  // get the directory path for both android and ios
  // to store the data in the database

    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + '/student.db';
  // after getting the path now create the database at the given path
    var studentsDatabase =
        await openDatabase(path, version: 1, onCreate: _createDataBase);
    return studentsDatabase;
  }

  void _createDataBase(Database db, int newVersion) async {
    // the ''' used for multi line strings
    await db.execute('''Create TABLE tbl_student(
   id INTEGER PRIMARY KEY AUTOINCREMENT,
   name TEXT,
   fName TEXT,
   rollNo INTEGER )
   ''');
  }

  // now create a method for insertion for the students.
  Future<int> insertStudent(StudentModel studentModel) async {
    Database database = await instance.database;
    var result = database.insert('tbl_student', studentModel.toMap());
    return result;
  }

  // now create a method to get all the students details
  Future<List<StudentModel>> getAllStudents() async {
    var db = await instance.database;
    // this method will return list of  map
    var result = await db.query('tbl_student');
    // now we need an empty list to store students
    List<StudentModel> students = [];
    // now get each of the student from the result
    for (var jsonStudent in result) {
      var std = StudentModel.fromMap(jsonStudent);
      students.add(std);
    }
    return students;
  }

  // now create a method for deletion of data from the database
  Future<int> deleteStudent(int id) async {
    // get the instance of the database first
    Database database = await instance.database;
    //if there is more name=? course=? So in [id, name,course]
    var result = database
        .delete('tbl_student', where: 'id=?', whereArgs: [id.toString()]);
    return result;
  }

  // now create a method for updating the data
  Future<int> updateStudent(StudentModel studentModel) async {
    // again get the instances of the database
    Database database = await instance.database;
    // now update the student data
    return await database.update('tbl_student', studentModel.toMap(),
        where: 'id=?', whereArgs: [studentModel.id.toString()]);
  }
}
