import 'package:flutter/material.dart';
import 'package:sql_concept/model/student_model.dart';
class StudentListWidget extends StatelessWidget {
  final StudentModel studentModel;
  final VoidCallback update;
  final VoidCallback delete;
  const StudentListWidget({Key? key, required this.studentModel, required this.update, required this.delete,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.indigo,
      margin: const EdgeInsets.all(10),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
        side: const BorderSide(color: Colors.red, width: 2),
      ),
      child:  Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        padding: const EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 5,
            ),
            const Text('Students Information',style: TextStyle(fontSize: 20),textAlign: TextAlign.center, ),
            ListTile(
              leading: Text('Name : ${studentModel.name}',style: const TextStyle(fontSize: 20),),
              trailing: Text('F/Name : ${studentModel.fName}',style: const TextStyle(fontSize: 20),),
            ),
            ListTile(
              leading: const Text('Roll No :',style: TextStyle(fontSize: 20),),
              trailing: Text('${studentModel.rollNo}',style: const TextStyle(fontSize: 20),),
            ),
            Row(
              children: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red,
                    onPrimary: Colors.black,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                      side: const BorderSide(
                        color: Colors.black,
                        width: 2
                      )
                    )
                  ),
                  onPressed: delete, child: const Text('Delete Data'),),
                const SizedBox(
                  width: 10,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      onPrimary: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                        side: const BorderSide(
                          color: Colors.black,
                          width: 2,
                        )
                      )
                    ),
                    onPressed: update, child: const Text('Update Data',))
              ],
            )
          ],
        ),
      ),
    );
  }
}
