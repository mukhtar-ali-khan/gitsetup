class StudentModel {
  int? id;
  String name;
  String fName;
  String rollNo;

  StudentModel({
    this.id,
    required this.name,
    required this.fName,
    required this.rollNo,
  });
   // to add students to sql we send map
   // so we have to convert dart obj into map
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'fName': fName,
      'rollNo': rollNo,
    };
  }

// extract a student obj to map obj
  static StudentModel fromMap(Map<String, dynamic> map) {
    return StudentModel(
      id: map['id'],
      name: map['name'],
      fName: map['fName'],
      rollNo: map['rollNo'],
    );
  }
}
